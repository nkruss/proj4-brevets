Author - Noah Kruss

Contact info - nkruss@uoregon.edu

# Project Description:

A reimplement the RUSA ACP control time calculator using flask and ajax.

When run the project runs a web application is started using flask where the user can input specified brevet distances, start time and control distances into a table. The web application takes these inputs and updates the web page dynamically using ajax adding in calculated control open and close times.

The open and close functions are based off of the ACP brevet control time rules given here (https://rusa.org/pages/acp-brevet-control-times-calculator) using the french rules. Controls are points where a rider must obtain proof of passage, and control[e] times are the minimum and maximum times by which the rider must arrive at the location.


## Additional notes

If the user inputs a invalid control distance such as the control distance being negative or the control distance begin more then 20% larger then the brevet distance, then the appropriate error message will be displayed in the open and close columns

If the user inputs a control distance that is not a number, then the open and close time columns will remain blank until the control distance is updated to a number value.

The implementation for open and close times will return times based off PST (west coast time) and do not take into account daylight savings
