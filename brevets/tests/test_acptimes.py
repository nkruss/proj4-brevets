"""
Nose tests for acp_times

"""
from acp_times import open_time
from acp_times import close_time
import arrow

import nose    # Testing framework
import logging
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.WARNING)
log = logging.getLogger(__name__)

def test_zero_cond():
    """Test that when control_dist_km is 0 that open time is brevet_start_time
    and that close time is (brevet_start_time + 1hr)
    """
    brevet_start_time = arrow.get("2020-01-01T00:00:00.000000-08:00")
    expected_open = brevet_start_time.isoformat()
    expected_close = brevet_start_time.shift(hours=+1).isoformat()

    control_dist_km = 0.0
    brevet_dist_km = 200.0
    brevet_start_time = brevet_start_time.isoformat()

    print(expected_open)
    print(expected_close)

    assert open_time(control_dist_km, brevet_dist_km, brevet_start_time) == expected_open
    assert close_time(control_dist_km, brevet_dist_km, brevet_start_time) == expected_close

def test_example_1():
    """
    Test that code matches example 1 from
    https://rusa.org/pages/acp-brevet-control-times-calculator
    """
    brevet_start_time = arrow.get("2020-01-01T00:00:00.000000-08:00")
    print("brevet_start_time: ", brevet_start_time)

    brevet_dist_km = 200.0
    controls = [60, 120, 175, 205]
    expected_opens = [brevet_start_time.shift(hours=+1, minutes=+46),
                      brevet_start_time.shift(hours=+3, minutes=+32),
                      brevet_start_time.shift(hours=+5, minutes=+9),
                      brevet_start_time.shift(hours=+5, minutes=+53)
                      ]
    expected_closes = [brevet_start_time.shift(hours=+4, minutes=+00),
                       brevet_start_time.shift(hours=+8, minutes=+00),
                       brevet_start_time.shift(hours=+11, minutes=+40),
                       brevet_start_time.shift(hours=+13, minutes=+30)
                       ]

    brevet_start_time = brevet_start_time.isoformat()
    for i in range(len(controls)):
        control = float(controls[i])
        expected_open = expected_opens[i].isoformat()
        expected_close = expected_closes[i].isoformat()

        print("\nControl_dist - ", control)
        print("expected open:     ", expected_open)
        assert open_time(control, brevet_dist_km, brevet_start_time) == expected_open
        print("expected close:    ", expected_close)
        assert close_time(control, brevet_dist_km, brevet_start_time) == expected_close

def test_example_2():
    """
    Test that code matches example 2 from
    https://rusa.org/pages/acp-brevet-control-times-calculator

    check for edge conditions for transition between control dist ranges
    """
    brevet_start_time = arrow.get("2020-01-01T00:00:00.000000-08:00")
    print("brevet_start_time: ", brevet_start_time)

    brevet_dist_km = 600.0
    controls = [0, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 609]
    expected_opens = [brevet_start_time.shift(hours=+0, minutes=+0),
                      brevet_start_time.shift(hours=+1, minutes=+28),
                      brevet_start_time.shift(hours=+2, minutes=+56),
                      brevet_start_time.shift(hours=+4, minutes=+25),
                      brevet_start_time.shift(hours=+5, minutes=+53),
                      brevet_start_time.shift(hours=+7, minutes=+27),
                      brevet_start_time.shift(hours=+9, minutes=+0),
                      brevet_start_time.shift(hours=+10, minutes=+34),
                      brevet_start_time.shift(hours=+12, minutes=+8),
                      brevet_start_time.shift(hours=+13, minutes=+48),
                      brevet_start_time.shift(hours=+15, minutes=+28),
                      brevet_start_time.shift(hours=+17, minutes=+8),
                      brevet_start_time.shift(hours=+18, minutes=+48),
                      ]
    expected_closes = [brevet_start_time.shift(hours=+1, minutes=+0),
                      brevet_start_time.shift(hours=+3, minutes=+30),
                      brevet_start_time.shift(hours=+6, minutes=+40),
                      brevet_start_time.shift(hours=+10, minutes=+0),
                      brevet_start_time.shift(hours=+13, minutes=+20),
                      brevet_start_time.shift(hours=+16, minutes=+40),
                      brevet_start_time.shift(hours=+20, minutes=+0),
                      brevet_start_time.shift(hours=+23, minutes=+20),
                      brevet_start_time.shift(hours=+26, minutes=+40),
                      brevet_start_time.shift(hours=+30, minutes=+0),
                      brevet_start_time.shift(hours=+33, minutes=+20),
                      brevet_start_time.shift(hours=+36, minutes=+40),
                      brevet_start_time.shift(hours=+40, minutes=+0),
                      ]

    brevet_start_time = brevet_start_time.isoformat()
    for i in range(len(controls)):
        control = float(controls[i])
        expected_open = expected_opens[i].isoformat()
        expected_close = expected_closes[i].isoformat()

        print("\nControl_dist - ", control)
        print("expected open:     ", expected_open)
        assert open_time(control, brevet_dist_km, brevet_start_time) == expected_open
        print("expected close:    ", expected_close)
        assert close_time(control, brevet_dist_km, brevet_start_time) == expected_close

def test_long_brevet():
    """
    Test for a 1000km long brevet

    (check that all boundary conditions between dist ranges function properly)
    """
    brevet_start_time = arrow.get("2020-01-01T00:00:00.000000-08:00")
    print("brevet_start_time: ", brevet_start_time)

    brevet_dist_km = 1000.0
    controls = [100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1010]
    expected_opens = [brevet_start_time.shift(hours=+2, minutes=+56),
                      brevet_start_time.shift(hours=+5, minutes=+53),
                      brevet_start_time.shift(hours=+9, minutes=+0),
                      brevet_start_time.shift(hours=+12, minutes=+8),
                      brevet_start_time.shift(hours=+15, minutes=+28),
                      brevet_start_time.shift(hours=+18, minutes=+48),
                      brevet_start_time.shift(hours=+22, minutes=+22),
                      brevet_start_time.shift(hours=+25, minutes=+57),
                      brevet_start_time.shift(hours=+29, minutes=+31),
                      brevet_start_time.shift(hours=+33, minutes=+5),
                      brevet_start_time.shift(hours=+33, minutes=+5)
                      ]
    expected_closes = [brevet_start_time.shift(hours=+6, minutes=+40),
                      brevet_start_time.shift(hours=+13, minutes=+20),
                      brevet_start_time.shift(hours=+20, minutes=+0),
                      brevet_start_time.shift(hours=+26, minutes=+40),
                      brevet_start_time.shift(hours=+33, minutes=+20),
                      brevet_start_time.shift(hours=+40, minutes=+0),
                      brevet_start_time.shift(hours=+48, minutes=+45),
                      brevet_start_time.shift(hours=+57, minutes=+30),
                      brevet_start_time.shift(hours=+66, minutes=+15),
                      brevet_start_time.shift(hours=+75, minutes=+0),
                      brevet_start_time.shift(hours=+75, minutes=+0)
                      ]

    brevet_start_time = brevet_start_time.isoformat()
    for i in range(len(controls)):
        control = float(controls[i])
        expected_open = expected_opens[i].isoformat()
        expected_close = expected_closes[i].isoformat()

        print("\nControl_dist - ", control)
        print("expected open:     ", expected_open)
        assert open_time(control, brevet_dist_km, brevet_start_time) == expected_open
        print("expected close:    ", expected_close)
        assert close_time(control, brevet_dist_km, brevet_start_time) == expected_close

def test_control_past_end():
    """
    Test that when control_dist_km is more then 20 percent greater then the
    brevet_dist_km that an error string will be produced
    """
    brevet_start_time = arrow.get("2020-01-01T00:00:00.000000-08:00")
    expected_open = "Error: control to far"
    expected_close = "Error: control to far"

    control_dist_km = 300.0
    brevet_dist_km = 200.0
    brevet_start_time = brevet_start_time.isoformat()

    print(expected_open)
    print(expected_close)

    assert open_time(control_dist_km, brevet_dist_km, brevet_start_time) == expected_open
    assert close_time(control_dist_km, brevet_dist_km, brevet_start_time) == expected_close

def test_control_at_20_percent():
    """
    Test that when control_dist_km is exactly more then 20 percent greater then the
    brevet_dist_km that no error message is produced
    """
    brevet_start_time = arrow.get("2020-01-01T00:00:00.000000-08:00")
    expected_open =  brevet_start_time.shift(hours=+5, minutes=+53).isoformat()
    expected_close = brevet_start_time.shift(hours=+13, minutes=+30).isoformat()

    control_dist_km = 240.0
    brevet_dist_km = 200.0
    brevet_start_time = brevet_start_time.isoformat()

    print(expected_open)
    print(expected_close)

    assert open_time(control_dist_km, brevet_dist_km, brevet_start_time) == expected_open
    assert close_time(control_dist_km, brevet_dist_km, brevet_start_time) == expected_close

def test_invalid_inputs():
    """
    Test that when control_dist_km negative that the correct error message is produced
    """

    brevet_start_time = arrow.get("2020-01-01T00:00:00.000000-08:00")
    expected_open = "Error: control < 0"
    expected_close = "Error: control < 0"

    control_dist_km = -5.0
    brevet_dist_km = 200.0
    brevet_start_time = brevet_start_time.isoformat()

    print(expected_open)
    print(expected_close)

    assert open_time(control_dist_km, brevet_dist_km, brevet_start_time) == expected_open
    assert close_time(control_dist_km, brevet_dist_km, brevet_start_time) == expected_close
