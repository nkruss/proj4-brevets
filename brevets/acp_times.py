"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow
import math

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.

    If inputs are invalid will return an error message string
    """
    #create a table to store speeds relating to each control dist
    ## values are in the form <control_dist_max: (min_speed, max_speed)>
    control_dist_speed_table = {200: (15, 34),
                                400: (15, 32),
                                600: (15, 30),
                                1000: (11.428, 28),
                                1300: (13.333, 26),
                                }

    #Input error conditions
    ##negative input error
    if control_dist_km < 0:
        return "Error: control < 0"
    ##condition of control being to far
    percent_diff = (control_dist_km - brevet_dist_km) / brevet_dist_km
    #resopnse for bad input
    if control_dist_km == 1e5:
        return ""
    elif (percent_diff > .2):
        return "Error: control to far"

    #20 percent condition
    elif (control_dist_km > brevet_dist_km) and (percent_diff <= .2):
        control_dist_km = brevet_dist_km


    time = 0
    dist_key_prev = 0
    dist_range = 200
    for dist_key in control_dist_speed_table:

        #add up times from previous segments in different timeing ranges
        if dist_key_prev != 0:
            prev_speeds = control_dist_speed_table[dist_key_prev]
            time += dist_range / prev_speeds[1]

        #found proper distance range
        if control_dist_km <= dist_key:
            speeds = control_dist_speed_table[dist_key]
            break

        #calculate size of previous segment
        dist_range = dist_key - dist_key_prev
        dist_key_prev = dist_key


    #add segment time to time to get to segment
    time += (control_dist_km  - dist_key_prev) / speeds[1]
    #get hour and min components of time rounded to nearest whole minute
    hrs = math.floor(time)
    mins = round((time % 1) * 60)

    #convert time string into arrow and update time
    opening_time = arrow.get(brevet_start_time)
    opening_time = opening_time.shift(hours=+hrs, minutes=+mins).isoformat()

    #print("calc open:         ", opening_time)
    return opening_time


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.

     Assuming that in the condition where control_dist_km is equal to or greater
     then brevet_dist_km then close time will be based off of the brevet end
     time rules given in https://rusa.org/pages/rulesForRiders article 9

     If inputs are invalid will return an error message string
    """
    #create a table to store speeds relating to each control dist
    ## values are in the form <control_dist_max: (min_speed, max_speed)>
    control_dist_speed_table = {200: (15, 34),
                                400: (15, 32),
                                600: (15, 30),
                                1000: (11.428, 28),
                                1300: (13.333, 26),
                                }

    #brevent end Times
    set_end_times = {200: 13.5,
                     300: 20.0,
                     400: 27.0,
                     600: 40.0,
                     1000: 75.0
                     }

    percent_diff = (control_dist_km - brevet_dist_km) / brevet_dist_km

    #convert time string into arrow
    closing_time = arrow.get(brevet_start_time)

    #input error conditions
    ##resopnse for bad control_dist input
    if control_dist_km == 1e5:
        return ""
    ##control to far condition
    elif (percent_diff > .2):
        return "Error: control to far"
    ##negative input error
    elif control_dist_km < 0:
        return "Error: control < 0"

    #french rule (includes zero condition)
    elif control_dist_km <= 60:
        time = (control_dist_km / 20) + 1
        closing_time = closing_time.shift(hours=+time).isoformat()

    #end brevet condition
    elif (percent_diff >= 0):
        time = set_end_times[brevet_dist_km]
        closing_time = closing_time.shift(hours=+time).isoformat()

    else:
        time = 0
        dist_key_prev = 0
        dist_range = 200
        for dist_key in control_dist_speed_table:

            #add up times from previous segments in different timeing ranges
            if dist_key_prev != 0:
                prev_speeds = control_dist_speed_table[dist_key_prev]
                time += dist_range / prev_speeds[0]
                #print(time)

            if control_dist_km <= dist_key:
                speeds = control_dist_speed_table[dist_key]
                break

            #calculate size of previous segment
            dist_range = dist_key - dist_key_prev
            dist_key_prev = dist_key

        #add segment time to time to get to segment
        time += (control_dist_km  - dist_key_prev) / speeds[0]
        #get hour and min components of time rounded to nearest whole minute
        hrs = math.floor(time)
        mins = (time % 1) * 60
        mins = round(mins)

        #add calculated time to closing_time
        closing_time = closing_time.shift(hours=+hrs, minutes=+mins).isoformat()

    #print("calc close:        ", closing_time)
    return closing_time
